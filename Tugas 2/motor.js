
class Motor {
    constructor(model, warna, merk, tahun){
        this.model = model;
        this.warna = warna;
        this.merk = merk;
        this.tahun = tahun;


    }

    // kecepatan
    drive(speed){

        if(speed >= 1){
            console.log(`kecepatan motor kamu : ${speed} km/jam`);
        }
        else{
            console.log('Motor kamu Berhenti');
        }    
        
    }

    // mundur
    reverse(drive){

        if (drive == true){
            console.log("Motor kamu mundur");
        }

        else{
            console.log("Motor kamu maju");
        }
    }

    kondisi(kondisi, jarak, km){
        const total = jarak+km;
        console.log(`kondisi motor anda ${kondisi} dengan total perjalanan mencapai ${total}km `);
    }

    tujuan(asal, tujuan, jarak){
        console.log(`tujuan awal anda ${asal} dan tujuan akhir anda ada di ${tujuan} dengan perkiraan jarak ${jarak}km `);
    }

    perkiraan(speed, jarak){
        const hasil = jarak/speed*60;
        console.log(`dengan jarak ${jarak}km dan kecepatan motor anda ${speed}km/jam maka perkiraan waktu tempuh anda adalah ${hasil} menit `);

    }



}

module.exports = Motor